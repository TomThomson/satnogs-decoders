---
meta:
  id: sonate2
  endian: be
doc: |
  :field src_callsign: ax25_frame.ax25_header.src_callsign_raw.callsign_ror.callsign
  :field o1uptm: ax25_frame.payload.ax25_info.space.source.telemetry.data.o1uptm
  :field p5v1av_5batvoltage: ax25_frame.payload.ax25_info.space.source.telemetry.data.p5v1av_value
  :field p5v1uv_5regvoltage: ax25_frame.payload.ax25_info.space.source.telemetry.data.p5v1uv_value
  :field p5v1bv_5vbusvoltage: ax25_frame.payload.ax25_info.space.source.telemetry.data.p5v1bv_value


seq:
  - id: ax25_frame
    type: ax25_frame
    doc-ref: 'https://www.tapr.org/pub_ax25.html'

types:
  ax25_frame:
    seq:
      - id: ax25_header
        type: ax25_header
      - id: payload
        type: ui_frame

  ax25_header:
    seq:
      - id: dest_callsign_raw
        type: callsign_raw
      - id: dest_ssid_raw
        type: ssid_mask
      - id: src_callsign_raw
        type: callsign_raw
      - id: src_ssid_raw
        type: ssid_mask
      - id: ctl
        type: b8
      - id: pid
        type: b8

  callsign_raw:
    seq:
      - id: callsign_ror
        process: ror(1)
        size: 6
        type: callsign

  callsign:
    seq:
      - id: callsign
        type: str
        encoding: ASCII
        size: 6

  ssid_mask:
    seq:
      - id: ssid_mask
        type: b8
    instances:
      ssid:
        value: (ssid_mask & 0x0f) >> 1

  ui_frame:
    seq:
      - id: ax25_info
        type: transferframe

  transferframe:
    seq:
      - id: tfvn
        type: b2
      - id: scid
        type: b10
      - id: vcidb
        type: b3
      - id: ocff
        type: b1
      - id: mcfc
        type: b8
      - id: vcfc
        type: b8
      - id: tf_shf
        type: b1
      - id: synchflag
        type: b1
      - id: pof
        type: b1
      - id: slid
        type: b2
      - id: fhp
        type: b11
      #- id: header_shift
      #  size: fhp.value
      - id: space
        type: spacepackage
      #- id: telemetry
      #  size-eos: true
        #type:
          #switch-on: source.apid.value
          #cases: 
            #1: telemetrypacket1
      - id: fecfd
        type: b16
        
  spacepackage:
    seq:
      - id: header
        type: spacepackage_header
      - id: source
        type: sourcepackage

  spacepackage_header:
    seq:
      - id: pvn
        type: b3
      - id: pt
        type: b1
      - id: shf
        type: b1
      - id: apid
        type: b11
      - id: seqflag
        type: b2
      - id: psc
        type: b14
      - id: pdl
        type: b16
    instances:    
      apid_value:
        value: apid
        
        
  sourcepackage:
    seq:
      - id: telemetry
        doc-ref: 'https://www.informatik.uni-wuerzburg.de/fileadmin/1003-ifex/2024/SONATE-2_protocol_definition_for_radio_amateurs.xlsx'
        type:
          switch-on: _parent.header.apid_value
          cases:
            100: apid100
            200: apid200
            _: rec_type_unknown

  rec_type_unknown:
    seq:
      - id: data
        size: 20
  
  apid200:
    seq:
      - id: header
        type: secondary_header5
      - id: data
        type: apid200_data
        
  apid100:
    seq:
      - id: header
        type: secondary_header5
      - id: data
        type: apid100_data_openai

  secondary_header5:
    seq:
      - id: ts
        type: b32
      - id: crc_ok
        type: b1
      - id: int
        type: b7
        
  apid100_data_openai:
    seq:
      - id: o1uptm
        type: b24
      - id: o1rstc
        type: b6
      - id: o1cmdc
        type: b4
      - id: o1pwrs
        type: b1
      - id: o1role
        type: b3
      - id: o1ecnt
        type: b6
      - id: o1ecde
        type: b6
      - id: o2uptm
        type: b24
      - id: o2rstc
        type: b6
      - id: o2cmdc
        type: b4
      - id: o2pwrs
        type: b1
      - id: o2role
        type: b3
      - id: o2ecnt
        type: b6
      - id: o2ecde
        type: b6
      - id: o3uptm
        type: b24
      - id: o3rstc
        type: b6
      - id: o3cmdc
        type: b4
      - id: o3pwrs
        type: b1
      - id: o3role
        type: b3
      - id: o3ecnt
        type: b6
      - id: o3ecde
        type: b6
      - id: o4uptm
        type: b24
      - id: o4rstc
        type: b6
      - id: o4cmdc
        type: b4
      - id: o4pwrs
        type: b1
      - id: o4role
        type: b3
      - id: o4ecnt
        type: b6
      - id: o4ecde
        type: b6
      - id: pdhhwi
        type: b3
      - id: cmdacn
        type: b12
      - id: cmdpcn
        type: b12
      - id: cmdeec
        type: b4
      - id: cmdeed
        type: b4
      - id: cmdsrc
        type: b4
      - id: cmdece
        type: b4
      - id: can1rs
        type: b4
      - id: can1ts
        type: b4
      - id: can2rs
        type: b4
      - id: can2ts
        type: b4
      - id: obmode
        type: b4
      - id: tmsink
        type: b4
      - id: gpcthw
        type: b3
      - id: swuphw
        type: b3
      - id: swinhw
        type: b3
      - id: sbcthw
        type: b3
      - id: cmdcnt
        type: b12
      - id: vhf1mo
        type: b3
      - id: vhf1tc
        type: b5
      - id: vhf1tm
        type: b8
      - id: vhf1rs
        type: b8
      - id: vhf1ra
        type: b8
      - id: vhf1to
        type: b4
      - id: vhf1ec
        type: b5
      - id: vhf1cc
        type: b4
      - id: vhf1en
        type: b4
      - id: vhf1rc
        type: b5
      - id: vhf2mo
        type: b3
      - id: vhf2tc
        type: b5
      - id: vhf2tm
        type: b8
      - id: vhf2rs
        type: b8
      - id: vhf2ra
        type: b8
      - id: vhf2to
        type: b4
      - id: vhf2ec
        type: b5
      - id: vhf2cc
        type: b4
      - id: vhf2en
        type: b4
      - id: vhf2rc
        type: b5
      - id: uhf1mo
        type: b3
      - id: uhf1tm
        type: b8
      - id: uhf1rs
        type: b8
      - id: uhf1ra
        type: b8
      - id: uhf1to
        type: b4
      - id: uhf1ec
        type: b5
      - id: uhf1cc
        type: b4
      - id: uhf1en
        type: b4
      - id: uhf1rc
        type: b5
      - id: uhf2mo
        type: b3
      - id: uhf2tm
        type: b8
      - id: uhf2rs
        type: b8
      - id: uhf2ra
        type: b8
      - id: uhf2to
        type: b4
      - id: uhf2ec
        type: b5
      - id: uhf2cc
        type: b4
      - id: uhf2en
        type: b4
      - id: uhf2rc
        type: b5
      - id: p5v1cc
        type: b4
      - id: p5v1pg
        type: b1
      - id: p5v1pf
        type: b1
      - id: p5v1ue
        type: b1
      - id: p5v1pe
        type: b1
      - id: p5v1eo
        type: b4
      - id: p5v1ec
        type: b4
      - id: p5v1av
        type: b8
      - id: p5v1ac
        type: b8
      - id: p5v1uv
        type: b8
      - id: p5v1uc
        type: b8
      - id: p5v1bv
        type: b8
      - id: p5v1bc
        type: b8
      - id: p5v2cc
        type: b4
      - id: p5v2pg
        type: b1
      - id: p5v2pf
        type: b1
      - id: p5v2ue
        type: b1
      - id: p5v2pe
        type: b1


      - id: p5v2eo
        type: b4
      - id: p5v2ec
        type: b4
      - id: p5v2av
        type: b8
      - id: p5v2ac
        type: b8
      - id: p5v2uv
        type: b8
      - id: p5v2uc
        type: b8
      - id: p5v2bv
        type: b8
      - id: p5v2bc
        type: b8
      - id: p5v1bt
        type: b8
      - id: aa12v1bt
        type: b8
      - id: sxp1ps
        type: b1
      - id: sxn1ps
        type: b1
      - id: syp1ps
        type: b1
      - id: syn1ps
        type: b1
      - id: szp1ps
        type: b1
      - id: szn1ps
        type: b1
      - id: ads1ps
        type: b1
      - id: mvw1ps
        type: b1
      - id: thr1ps
        type: b1
      - id: ifp1ps
        type: b1
      - id: p5v1ps
        type: b1
      - id: p5v1ha
        type: b1
      - id: thr1pg
        type: b1
      - id: p5v1rs
        type: b1
      - id: p5v1cs
        type: b1
      - id: p5v111
        type: b1
      - id: p5v121
        type: b1
      - id: p5v122
        type: b1
      - id: p5v1tr
        type: b1
      - id: p5v1rm
        type: b1
      - id: p5v2bt
        type: b8
      - id: a12v2bt
        type: b8
      - id: sxp2ps
        type: b1
      - id: sxn2ps
        type: b1
      - id: syp2ps
        type: b1
      - id: syn2ps
        type: b1
      - id: szp2ps
        type: b1
      - id: szn2ps
        type: b1
      - id: ads2ps
        type: b1
      - id: mvw2ps
        type: b1
      - id: thr2ps
        type: b1
      - id: ifp2ps
        type: b1
      - id: p5v2ps
        type: b1
      - id: p5v2ha
        type: b1
      - id: thr2pg
        type: b1
      - id: p5v2rs
        type: b1
      - id: p5v2cs
        type: b1
      - id: p5v211
        type: b1
      - id: p5v221
        type: b1
      - id: p5v222
        type: b1
      - id: p5v2tr
        type: b1
      - id: p5v2rm
        type: b1
      - id: a12v1cc
        type: b4
      - id: a12v1pg
        type: b1
      - id: a12v1pf
        type: b1
      - id: a12v1ue
        type: b1
      - id: a12v1pe
        type: b1
      - id: a12v1eo
        type: b4
      - id: a12v1ec
        type: b4
      - id: a12v1av
        type: b8
      - id: a12v1ac
        type: b8
      - id: a12v1uv
        type: b8
      - id: a12v1uc
        type: b8
      - id: a12v1bv
        type: b8
      - id: a12v1bc
        type: b8
      - id: a12v2cc
        type: b4
      - id: a12v2pg
        type: b1
      - id: a12v2pf
        type: b1
      - id: a12v2ue
        type: b1
      - id: a12v2pe
        type: b1
      - id: a12v2eo
        type: b4
      - id: a12v2ec
        type: b4
      - id: a12v2av
        type: b8
      - id: a12v2ac
        type: b8
      - id: a12v2uv
        type: b8
      - id: a12v2uc
        type: b8
      - id: a12v2bv
        type: b8
      - id: a12v2bc
        type: b8
      - id: a12v1ss
        type: b2
      - id: ai1pws
        type: b1
      - id: a12v1ha
        type: b1
      - id: a12v1cs
        type: b1
      - id: a12v1rm
        type: b1
      - id: a12v2ss
        type: b2
      - id: ai2pws
        type: b1
      - id: a12v2ha
        type: b1
      - id: a12v2cs
        type: b1
      - id: a12v2rm
        type: b1
      - id: ifp1st
        type: b2
      - id: sb1rxs
        type: b1
      - id: sb1ena
        type: b1
      - id: rwx1ps
        type: b1
      - id: rwy1ps
        type: b1
      - id: rwz1ps
        type: b1
      - id: sb1txs
        type: b1
      - id: ifp1en
        type: b4
      - id: ifp1ec
        type: b4
      - id: ifp1cc
        type: b4
      - id: ifp1ac
        type: b4
      - id: ifp2st
        type: b2
      - id: sb2rxs
        type: b1
      - id: sb2ena
        type: b1
      - id: rwx2ps
        type: b1
      - id: rwy2ps
        type: b1
      - id: rwz2ps
        type: b1
      - id: sb2txs
        type: b1
      - id: ifp2en
        type: b4
      - id: ifp2ec
        type: b4
      - id: ifp2cc
        type: b4
      - id: ifp2ac
        type: b4
    instances:
      p5v1av_value:
        value: p5v1av * 0.020070588
      p5v1uv_value:
        value: p5v1uv * 0.020813943
      p5v1bv_value:
        value: p5v1bv * 0.024530719

  apid100_data:
    seq:
      - id: o1uptm
        type: b24
      - id: o1rstc
        type: b6
      - id: o1cmdc
        type: b4
      - id: o1pwrs
        type: b1
      - id: o1role
        type: b3
      - id: o1ecnt
        type: b6
      - id: o1ecde
        type: b6
        
      - id: o2uptm
        type: b24
      - id: o2rstc
        type: b6
      - id: o2cmdc
        type: b4
      - id: o2pwrs
        type: b1
      - id: o2role
        type: b3
      - id: o2ecnt
        type: b6
      - id: o2ecde
        type: b6
        
      - id: o3uptm
        type: b24
      - id: o3rstc
        type: b6
      - id: o3cmdc
        type: b4
      - id: o3pwrs
        type: b1
      - id: o3role
        type: b3
      - id: o3ecnt
        type: b6
      - id: o3ecde
        type: b6
        
      - id: o4uptm
        type: b24
      - id: o4rstc
        type: b6
      - id: o4cmdc
        type: b4
      - id: o4pwrs
        type: b1
      - id: o4role
        type: b3
      - id: o4ecnt
        type: b6
      - id: o4ecde
        type: b6
        
      - id: pdhhwi
        type: b3
      - id: cmdacn
        type: b12
      - id: cmdpcn
        type: b12
      - id: cmdeec
        type: b4
      - id: cmdeed
        type: b4
      - id: cmdsrc
        type: b4
      - id: cmdece
        type: b4
        
      - id: can1rs
        type: b4
      - id: can2rs
        type: b4
      - id: can3rs
        type: b4
      - id: can4rs
        type: b4
        
      - id: obmode
        type: b4
      - id: tmsink
        type: b4
      - id: gpcthw
        type: b3
      - id: swuphw
        type: b3
      - id: swinhw
        type: b3
      - id: sbcthw
        type: b3
      - id: cmdcnt
        type: b12
        
      - id: vhf1mo
        type: b3
      - id: vhf1tc
        type: b5
      - id: vhf1tm
        type: b8
      - id: vhf1rs
        type: b8
      - id: vhf1ra
        type: b8
      - id: vhf1to
        type: b4
      - id: vhf1ec
        type: b5
      - id: vhf1cc
        type: b4
      - id: vhf1en
        type: b4
      - id: vhf1rc
        type: b5
        
      - id: vhf2mo
        type: b3
      - id: vhf2tc
        type: b5
      - id: vhf2tm
        type: b8
      - id: vhf2rs
        type: b8
      - id: vhf2ra
        type: b8
      - id: vhf2to
        type: b4
      - id: vhf2ec
        type: b5
      - id: vhf2cc
        type: b4
      - id: vhf2en
        type: b4
      - id: vhf2rc
        type: b5
        
      - id: uhf1mo
        type: b3
      - id: uhf1tm
        type: b8
      - id: uhf1rs
        type: b8
      - id: uhf1ra
        type: b8
      - id: uhf1to
        type: b4
      - id: uhf1ec
        type: b5
      - id: uhf1cc
        type: b4
      - id: uhf1en
        type: b4
      - id: uhf1rc
        type: b5
        
      - id: uhf2mo
        type: b3
      - id: uhf2tm
        type: b8
      - id: uhf2rs
        type: b8
      - id: uhf2ra
        type: b8
      - id: uhf2to
        type: b4
      - id: uhf2ec
        type: b5
      - id: uhf2cc
        type: b4
      - id: uhf2en
        type: b4
      - id: uhf2rc
        type: b5
        
      - id: p5v1cc
        type: b4
      - id: p5v1pg
        type: b1
      - id: p5v1pf
        type: b1
      - id: p5v1ue
        type: b1
      - id: p5v1pe
        type: b1
      - id: p5v1eo
        type: b4
      - id: p5v1ec
        type: b4
      - id: p5v1av
        type: b8
      - id: p5v1ac
        type: b8
      - id: p5v1uv
        type: b8
      - id: p5v1uc
        type: b8
      - id: p5v1bv
        type: b8
      - id: p5v1bc
        type: b8
        
      - id: p5v2cc
        type: b4
      - id: p5v2pg
        type: b1
      - id: p5v2pf
        type: b1
      - id: p5v2ue
        type: b1
      - id: p5v2pe
        type: b1
      - id: p5v2eo
        type: b4
      - id: p5v2ec
        type: b4
      - id: p5v2av
        type: b8
      - id: p5v2ac
        type: b8
      - id: p5v2uv
        type: b8
      - id: p5v2uc
        type: b8
      - id: p5v2bv
        type: b8
      - id: p5v2bc
        type: b8
      
      - id: p5v1bt
        type: b8
      - id: a12v1bt
        type: b8
        
      - id: rest_data
        size: 1
    instances:
      p5v1av_value:
        value: p5v1av * 0.020070588
      p5v1uv_value:
        value: p5v1uv * 0.020813943
      p5v1bv_value:
        value: p5v1bv * 0.024530719
        
  apid200_data:
    seq:
      - id: adcatc
        type: b1
      - id: adcmod
        type: b3
      - id: adcoco
        type: b4
      - id: adceco
        type: b8
      - id: adcecn
        type: b6
      - id: adfalg
        type: b2
      - id: adfimv
        type: b2
      - id: adcref
        type: b1
      - id: adcsr2
        type: b1
      - id: adfist
        type: b1
      - id: adfiss
        type: b3
      - id: adfrol
        type: b16
      - id: adfpit
        type: b16
      - id: adfyaw
        type: b16
      - id: mvirol
        type: b16
      - id: mvipit
        type: b16
      - id: mviyaw
        type: b16
      - id: mmx1bf
        type: b16
      - id: mmy1bf
        type: b16
      - id: mmz1bf
        type: b16
      - id: gyrrox
        type: b16
      - id: gyrroy
        type: b16
      - id: gyrroz
        type: b16
      - id: ssxmea
        type: b16
      - id: ssymea
        type: b16
      - id: sszmea
        type: b16
      - id: prealt
        type: b16
      - id: prelat
        type: b16
      - id: prelon
        type: b16
      - id: ttorqx
        type: b16
      - id: ttorqy
        type: b16
      - id: ttorqz
        type: b16
      - id: sunvis
        type: b1
      - id: adcatv
        type: b1
      - id: adcgux
        type: b2
      - id: adcguy
        type: b2
      - id: adcguz
        type: b2
        
  binary1:
    seq:
      - id: raw
        type: b1
    instances:
      value:
        value: raw
  binary2:
    seq:
      - id: raw
        type: b2
    instances:
      value:
        value: raw
  binary3:
    seq:
      - id: raw
        type: b3
    instances:
      value:
        value: raw
  binary4:
    seq:
      - id: raw
        type: b4
    instances:
      value:
        value: raw
  binary8:
    seq:
      - id: raw
        type: b8
    instances:
      value:
        value: raw
  binary10:
    seq:
      - id: raw
        type: b10
    instances:
      value:
        value: raw
  binary11:
    seq:
      - id: raw
        type: b11
    instances:
      value:
        value: raw
  binary12:
    seq:
      - id: raw
        type: b12
    instances:
      value:
        value: raw
  binary14:
    seq:
      - id: raw
        type: b14
    instances:
      value:
        value: raw
  binary15:
    seq:
      - id: raw
        type: b15
    instances:
      value:
        value: raw
  binary16:
    seq:
      - id: raw
        type: b16
    instances:
      value:
        value: raw
  binary32:
    seq:
      - id: raw
        type: b32
    instances:
      value:
        value: raw
  p5v1av_b8:
    seq:
      - id: raw
        type: b8
    instances:
      value:
        value: 203 * 0.020070588
